#!/bin/bash

name='no_gravity'

# arch='-mthumb-interwork -mthumb'
arch=''
specs='-specs=gba.specs'
flags="$arch $specs -O2 -fno-strict-aliasing"
libs=""

cc='/opt/devkitpro/devkitARM/bin/arm-none-eabi-gcc'
objcpy='/opt/devkitpro/devkitARM/bin/arm-none-eabi-objcopy'

time $cc src/game_main.c -o $name.elf $flags $libs
$objcpy -O binary $name.elf $name.gba
gbafix $name.gba

rm $name.elf

[[ $1 = 'run' ]] && mednafen $name.gba
