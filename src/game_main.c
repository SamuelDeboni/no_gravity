#include "gba_helper.h"
#include "input.h"
#include "fixed.h"
#include "sprites.h"

#include "sound.h"

ObjAttr obj_buffer[128];
ObjAffine *const obj_aff_buffer = (ObjAffine *)obj_buffer;
u32 sprite_count = 0;

static void
resetSpriteBuffer()
{
    for (i32 i = 0; i < sprite_count; i++) {
        obj_buffer[i] = (ObjAttr){
            .attr0 = 0x0200,
        };
    }
    
    sprite_count = 0;
}

static void
drawSprite(i32 x, i32 y, u32 tile_index, u32 h_flip, u32 size)
{
    if (sprite_count >= 128) return;
    obj_buffer[sprite_count] = (ObjAttr){.attr0 = 0x2000,};
    
    BF_SET(obj_buffer[sprite_count].attr0, y, ATTR0_Y);
    BF_SET(obj_buffer[sprite_count].attr1, x, ATTR1_X);
    
    BF_SET(obj_buffer[sprite_count].attr1, h_flip, ATTR1_HF);
    BF_SET(obj_buffer[sprite_count].attr1, size, ATTR1_SZ);
    
    BF_SET(obj_buffer[sprite_count].attr2, tile_index, ATTR2_ID);
    
    sprite_count++;
}

#include "player.c"

static u32
loadSprite(const u32 *sprite, u32 count, u32 *i)
{
    u32 result = (*i) * 2;
    memCopy32((u32 *) &tile8_mem[4][*i], sprite, count);
    *i += count / 16;
    return result;
}

int
main()
{
    // init sound
    initSound();
    
    // copy palettes
    memCopy32((u32 *) palette_mem, palette, 128);
    memCopy32((u32 *) bg_palette_mem, palette, 128);
    
    
    // copy sprites tiles
    {
        u32 i = 0;
        cube_spr_i       = loadSprite(SP_MaintenanceCube, SP_MaintenanceCube_count, &i);
        cube_spr_right_i = loadSprite(SP_MaintenanceCube_Right, SP_MaintenanceCube_Right_count, &i);
        cube_spr_down_i  = loadSprite(SP_MaintenanceCube_Down, SP_MaintenanceCube_Down_count, &i);
        cube_spr_left_i  = loadSprite(SP_MaintenanceCube_Left, SP_MaintenanceCube_Left_count, &i);
        cube_spr_up_i    = loadSprite(SP_MaintenanceCube_Up, SP_MaintenanceCube_Up_count, &i);
        cube_spr_jump_i  = loadSprite(SP_MaintenanceCube_Jump, SP_MaintenanceCube_Jump_count, &i);
        battery_spr_i    = loadSprite(SP_EmergencyBattery, SP_EmergencyBattery_count, &i);
    }
    
    // copy bg tiles
    memCopy32((u32 *) &tile8_mem[1][0], SP_ShipTS, SP_ShipTS_count);
    memCopy32((u32 *) &screen_block_mem[0], SB_Level_III, SB_Level_III_count);
    
    oamInit(obj_buffer, 128);
    REG_DISPCNT = DCNT_MODE0 | DCNT_BG0 | DCNT_OBJ | DCNT_OBJ_1D;
    
    // configure bg 1
    REG_BG0_CONTROL = 0x80; // Color depth 8bpp
    REG_BG0_CONTROL |= 0x4; // set CBB to 1
    BF_SET(REG_BG0_CONTROL, 0, BG_CONTROL_SBB);
    BF_SET(REG_BG0_CONTROL, 3, BG_CONTROL_SZ);
    
    Player player = {
        .x = 16,
        .y = 104,
        .frame = 0,
    };
    
    View view = {
        .x = 0,
        .y = 32,
    };
    
    
    Battery bat = {
        .x = 48,
        .y = 104,
        .vx = 0,
    };
    
    while (1) {
        waitVDraw();
        keyPoll();
        
        if (_sound_init_delay_counter > SND_INIT_DELAY) {
            playNotesCh1();
            playNotesCh2();
            playNotesCh4();
        } else {
            _sound_init_delay_counter++;
        }
        
        resetSpriteBuffer();
        
        REG_BG0_SCROLL_H = view.x;
        REG_BG0_SCROLL_V = view.y;
        
        { // Update view
            i32 screen_x = player.x - view.x;
            i32 screen_y = player.y - view.y;
            
            if (screen_x < 64 ) {
                view.x -= 1;
            } else if (screen_x > S_WIDTH - 80) {
                view.x += 1;
            }
            
            if (screen_y < 64) {
                view.y--;
            } else if (screen_y > S_HEIGHT - 80) {
                view.y++;
            }
            
            view.x = CLAMP(view.x, 0, 256 - S_WIDTH);
            view.y = CLAMP(view.y, 0, 256 - S_HEIGHT);
        }
        
        updatePlayer(&player, &bat, &view);
        updateBattery(&bat, &player, &view);
        
        //playNotesCh1();
        
        waitVBlank();
        oamCopy(oam_mem, obj_buffer, sprite_count);
    }
    
    return 0;
}
