#ifndef REGISTERS_H
#define REGISTERS_H

#include "types.h"
#define REG_BASE MEM_IO

// Sound Control
#define REG_SNDCNT    *(vu32*)(REG_BASE+0x0080)   //!< Main sound control
#define REG_SNDDMGCNT *(vu16*)(REG_BASE+0x0080)   //!< DMG channel control
#define REG_SNDDSCNT  *(vu16*)(REG_BASE+0x0082)   //!< Direct Sound control
#define REG_SNDSTAT   *(vu16*)(REG_BASE+0x0084)   //!< Sound status
#define REG_SNDBIAS   *(vu16*)(REG_BASE+0x0088)   //!< Sound bias

// Sound: Channel 1: Square wave + sweep
#define REG_SND1SWEEP       *(vu16*)(REG_BASE+0x0060)   //!< Channel 1 Sweep
#define REG_SND1CNT         *(vu16*)(REG_BASE+0x0062)   //!< Channel 1 Control
#define REG_SND1FREQ        *(vu16*)(REG_BASE+0x0064)   //!< Channel 1 frequency

// Sound: Channel 2: Simple square wave
#define REG_SND2CNT         *(vu16*)(REG_BASE+0x0068)   //!< Channel 2 control
#define REG_SND2FREQ        *(vu16*)(REG_BASE+0x006C)   //!< Channel 2 frequency

// Sound: Channel 3: Wave player
#define REG_SND3SEL         *(vu16*)(REG_BASE+0x0070)   //!< Channel 3 wave select
#define REG_SND3CNT         *(vu16*)(REG_BASE+0x0072)   //!< Channel 3 control
#define REG_SND3FREQ        *(vu16*)(REG_BASE+0x0074)   //!< Channel 3 frequency

// Sound: Channel 4: Noise generator
#define REG_SND4CNT         *(vu16*)(REG_BASE+0x0078)   //!< Channel 4 control
#define REG_SND4FREQ        *(vu16*)(REG_BASE+0x007C)   //!< Channel 4 frequency

// Sound: Buffers
#define REG_WAVE_RAM         (vu32*)(REG_BASE+0x0090)   //!< Channel 3 wave buffer

#define REG_WAVE_RAM0       *(vu32*)(REG_BASE+0x0090)
#define REG_WAVE_RAM1       *(vu32*)(REG_BASE+0x0094)
#define REG_WAVE_RAM2       *(vu32*)(REG_BASE+0x0098)
#define REG_WAVE_RAM3       *(vu32*)(REG_BASE+0x009C)

#define REG_FIFO_A          *(vu32*)(REG_BASE+0x00A0)   //!< DSound A FIFO
#define REG_FIFO_B          *(vu32*)(REG_BASE+0x00A4)   //!< DSound B FIFO

#endif
