/* date = January 16th 2021 3:02 pm */

#ifndef COMMON_H
#define COMMON_H

typedef unsigned char   u8;
typedef unsigned short u16;
typedef unsigned int   u32;
typedef unsigned long  u64;

typedef volatile  u8  vu8;
typedef volatile u16 vu16;
typedef volatile u32 vu32;
typedef volatile u64 vu64;

typedef char   i8;
typedef short i16;
typedef int   i32;
typedef long  i64;

typedef volatile  i8  vi8;
typedef volatile i16 vi16;
typedef volatile i32 vi32;
typedef volatile i64 vi64;

typedef u32 b32;

#define internal static
#define global static
#define persistent static

#define true 1
#define false 0

typedef u16 Color;

#define ALIGN(n)    __attribute__((aligned(n)))
#define PACKED      __attribute__((packed))

#define ALIGN4 ALIGN(4)


#endif //COMMON_H
