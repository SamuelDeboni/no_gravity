#ifndef SFX_H
#define SFX_H

#include "../sound.h"


/*
array constante de 48 valores.
*/


// 60 frames -> 1s
#define _hit_sfx_qnt 60
const u8 hit_sfx[_hit_sfx_qnt] = {
    C | OCT_1,
    C | OCT_1,
    C | OCT_1,
    C | OCT_1,
    C | OCT_1,
    NO_NOTE,
    NO_NOTE,
    NO_NOTE,
    NO_NOTE,
    C | OCT_1,
    C | OCT_1,
    C | OCT_1,
    NO_NOTE,
    NO_NOTE,
    NO_NOTE,
    NO_NOTE,
    NO_NOTE,
    NO_NOTE,
    NO_NOTE,
    C | OCT_1,
    
    C | OCT_1,
    C | OCT_1,
    NO_NOTE,
    NO_NOTE,
    NO_NOTE,
    NO_NOTE,
    NO_NOTE,
    NO_NOTE,
    NO_NOTE,
    C | OCT_1,
    C | OCT_1,
    C | OCT_1,
    C | OCT_1,
    C | OCT_1,
    C | OCT_1,
    NO_NOTE,
    NO_NOTE,
    NO_NOTE,
    NO_NOTE,
    C | OCT_1,

    C | OCT_1,
    C | OCT_1,
    NO_NOTE,
    NO_NOTE,
    NO_NOTE,
    NO_NOTE,
    NO_NOTE,
    NO_NOTE,
    NO_NOTE,
    C | OCT_1,
    C | OCT_1,
    C | OCT_1,
    NO_NOTE,
    NO_NOTE,
    NO_NOTE,
    NO_NOTE,
    NO_NOTE,
    NO_NOTE,
    NO_NOTE,
    C | OCT_1,
};

static inline void
hitSfx() {
    _actual_sound_eff = hit_sfx;
    _actual_sound_eff_tick_qnt = _hit_sfx_qnt;
}

#define _jump_sfx_qnt 20
const u8 jump_sfx[_jump_sfx_qnt] = {
    C | OCT_1,
    E | OCT_0,
    C | OCT_1,
    E | OCT_0,
    C | OCT_1,
    NO_NOTE,
    NO_NOTE,
    NO_NOTE,
    NO_NOTE,
    D | OCT_0,
    C | OCT_0,
    D | OCT_0,
    NO_NOTE,
    NO_NOTE,
    NO_NOTE,
    NO_NOTE,
    A | OCT_0,
    G | OCT_0,
    F | OCT_0,
    E | OCT_0,
};

static inline void
jumpSfx() {
    _actual_sound_eff = jump_sfx;
    _actual_sound_eff_tick_qnt = _jump_sfx_qnt;
}

#endif
