#ifndef MUSICS_H
#define MUSICS_H

#include "../sound.h"

#define BEAT  64
#define SLICE 8

#define notes_ch1_qnt 26

const u8 notes_ch1[notes_ch1_qnt][3] =
{
    C  | OCT_0, 1                      , 1,
    NO_NOTE   , (SLICE*2 - 1)          , 1,
    Ds | OCT_0, 1                      , 1,
    NO_NOTE   , (SLICE*2 - 1)          , 1,
    F  | OCT_0, 1                      , 4,
    NO_NOTE   , (BEAT*2 - SLICE*4 - 1) , 1, //6

    C  | OCT_0, 1                      , 1,
    NO_NOTE   , (SLICE*2 - 1)          , 1,
    Ds | OCT_0, 1                      , 1,
    NO_NOTE   , (SLICE*2 - 1)          , 1,
    As | OCT_0, 1                      , 1,
    NO_NOTE   , (SLICE*2 - 1)          , 1,
    Gs | OCT_0, 1                      , 4,
    NO_NOTE   , (BEAT*2 - SLICE*6  - 1), 1, //8

    C  | OCT_0, 1                      , 1,
    NO_NOTE   , (SLICE*2 - 1)          , 1,
    Ds | OCT_0, 1                      , 1,
    NO_NOTE   , (SLICE*2 - 1)          , 1,
    F  | OCT_0, 1                      , 4,
    NO_NOTE   , (BEAT*2 - SLICE*4 - 1) , 1, //6

	C  | OCT_0, 1                      , 1,
    NO_NOTE   , (SLICE*2 - 1)          , 1,
    C | OCT_0, 1                       , 1,
    NO_NOTE   , (SLICE*2 - 1)          , 1,
    C  | OCT_0, 1                      , 4,
    NO_NOTE   , (BEAT*2 - SLICE*4 - 1) , 1, //6
};

#define notes_ch2_qnt 8
const u8 notes_ch2[notes_ch2_qnt][2] =
{
    C  | OCT_0,  1,
    NO_NOTE, (BEAT*2 - 1),

    Ds | OCT_0,  1,
    NO_NOTE, (BEAT*2 - 1),

    F  | OCT_0,  1,
    NO_NOTE, (BEAT*2 - 1),

    G  | OCT_0,  1,
    NO_NOTE, (BEAT*2 - 1),
};

#define notes_ch4_qnt 18
const u8 notes_ch4[notes_ch4_qnt][2] =
{
    C  | OCT_1,  1,
    NO_NOTE, (BEAT - 1),
    C  | OCT_1,  1,
    NO_NOTE, (BEAT - 1),
    C  | OCT_1,  1,
    NO_NOTE, (BEAT - 1),
    C  | OCT_1,  1,              // double tap 
    NO_NOTE, (SLICE - 1),        //
    C  | OCT_1,  1,              //
    NO_NOTE, (BEAT - SLICE - 1), //

    C  | OCT_1,  1,
    NO_NOTE, (BEAT - 1),
    C  | OCT_1,  1,
    NO_NOTE, (BEAT - 1),
    C  | OCT_1,  1,
    NO_NOTE, (BEAT - 1),
    F  | OCT_1,  1,
    NO_NOTE, (BEAT - 1),
};

#endif
