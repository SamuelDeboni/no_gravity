#include "sound.h"
#include "sound/sfx.h"

#define PLAYER_FLIP    (1 << 0)
#define PLAYER_WALKING (1 << 1)
#define PLAYER_ON_AIR  (1 << 2)

typedef struct View {
    i32 x, y;
} ALIGN4 View;

typedef struct Player {
    i32 x;
    i32 y;
    i32 vx;
    i32 vy;
    
    u32 flags;
    u16 frame;
    u16 anim_timer;
} ALIGN4 Player;

typedef struct Battery {
    i32 x;
    i32 y;
    i32 vx;
    i32 vy;
    
    u32 grabed;
} ALIGN4 Battery;

#define COLLISION_UP    (1 << 3)
#define COLLISION_DOWN  (1 << 4)
#define COLLISION_RIGHT (1 << 5)
#define COLLISION_LEFT  (1 << 6)

static u32 cube_spr_i = 0;
static u32 cube_spr_right_i = 0;
static u32 cube_spr_down_i = 0;
static u32 cube_spr_left_i = 0;
static u32 cube_spr_up_i = 0;
static u32 cube_spr_jump_i = 0;
static u32 battery_spr_i = 0;

static inline b32
checkBGCollision(i32 x, i32 y, u32 sb_offset)
{
    u32 offset = sb_offset;
    
    if (y > 255) {
        offset += 1;
    }
    
    if (x > 255) {
        offset += 1;
        y -= 8;
    };
    
    x = x / 8;
    y = y / 8;
    
    u32 value = screen_block_mem[offset][x + y * 32];
    return (value > 0 && value < 128);
}

static u32
playerCollisionUp(i32 pos_x, i32 pos_y, u32 sb_offset)
{
    u16 *sb = &screen_block_mem[sb_offset][0];
    
    i32 xl = (pos_x + 3);
    i32 xm = xl + 8;
    i32 xr = (pos_x + 12);
    i32 y = (pos_y - 1);
    
    if ( checkBGCollision(xl, y, sb_offset) || checkBGCollision(xm, y, sb_offset) || checkBGCollision(xr, y, sb_offset)) return COLLISION_UP;
    
    return 0;
}


static u32
playerCollisionDown(i32 pos_x, i32 pos_y, u32 sb_offset)
{
    u16 *sb = &screen_block_mem[sb_offset][0];
    
    i32 xl = (pos_x + 3);
    i32 xm = xl + 8;
    i32 xr = (pos_x + 12);
    i32 y  = (pos_y + 16);
    
    if ( checkBGCollision(xl, y, sb_offset) || checkBGCollision(xm, y, sb_offset) || checkBGCollision(xr, y, sb_offset)) return COLLISION_DOWN;
    
    return 0;
}

static u32
playerCollisionLeft(i32 pos_x, i32 pos_y, u32 sb_offset)
{
    u16 *sb = &screen_block_mem[sb_offset][0];
    
    i32 yl = (pos_y + 3);
    i32 ym = yl + 8;
    i32 yr = (pos_y + 12);
    i32 x = (pos_x - 1);
    
    if ( checkBGCollision(x, yl, sb_offset) || checkBGCollision(x, ym, sb_offset) || checkBGCollision(x, yr, sb_offset)) return COLLISION_LEFT;
    
    return 0;
}

static u32
playerCollisionRight(i32 pos_x, i32 pos_y, u32 sb_offset)
{
    u16 *sb = &screen_block_mem[sb_offset][0];
    
    i32 yl = (pos_y + 3);
    i32 ym = yl + 8;
    i32 yr = (pos_y + 12);
    i32 x = (pos_x + 16);
    
    if ( checkBGCollision(x, yl, sb_offset) || checkBGCollision(x, ym, sb_offset) || checkBGCollision(x, yr, sb_offset)) return COLLISION_RIGHT;
    
    return 0;
}

static void
updatePlayer(Player *player, Battery *bat, View *view) {
    i32 input_x = keyAxis(0);
    i32 input_y = keyAxis(1);
    
    if (keyDown(KEY_A)) hitSfx();
    if (keyDown(KEY_B)) jumpSfx();

    if (input_x != 0 && input_y != 0) {
        player->flags |= PLAYER_WALKING;
    } else {
        player->flags &= ~PLAYER_WALKING;
    }
    
    u32 col = 0;
    
    switch (bat->grabed) {
        case 0: {
            col |= playerCollisionUp(player->x, player->y, 0);
            col |= playerCollisionDown(player->x, player->y, 0);
            col |= playerCollisionLeft(player->x, player->y, 0);
            col |= playerCollisionRight(player->x, player->y, 0);
        } break;
        
        case 1: {
            col |= playerCollisionUp(player->x, player->y, 0);
            col |= playerCollisionUp(player->x + 16, player->y, 0);
            col |= playerCollisionDown(player->x, player->y, 0);
            col |= playerCollisionDown(player->x + 16, player->y, 0);
            col |= playerCollisionRight(player->x + 16, player->y, 0);
            col |= playerCollisionLeft(player->x, player->y, 0);
        } break;
        
        case 2: {
            col |= playerCollisionUp(player->x, player->y, 0);
            col |= playerCollisionUp(player->x - 16, player->y, 0);
            col |= playerCollisionDown(player->x, player->y, 0);
            col |= playerCollisionDown(player->x - 16, player->y, 0);
            col |= playerCollisionRight(player->x, player->y, 0);
            col |= playerCollisionLeft(player->x - 16, player->y, 0);
        } break;
        
        case 3: {
            col |= playerCollisionUp(player->x, player->y, 0);
            col |= playerCollisionDown(player->x, player->y + 16, 0);
            col |= playerCollisionLeft(player->x, player->y, 0);
            col |= playerCollisionLeft(player->x, player->y + 16, 0);
            col |= playerCollisionRight(player->x, player->y, 0);
            col |= playerCollisionRight(player->x, player->y + 16, 0);
        } break;
        
        case 4: {
            col |= playerCollisionUp(player->x, player->y - 16, 0);
            col |= playerCollisionDown(player->x, player->y, 0);
            col |= playerCollisionLeft(player->x, player->y, 0);
            col |= playerCollisionLeft(player->x, player->y - 16, 0);
            col |= playerCollisionRight(player->x, player->y, 0);
            col |= playerCollisionRight(player->x, player->y - 16, 0);
        } break;
    }
    
    // Colision fix
    {
        if (col & COLLISION_UP) {
            if (player->y % 8 != 0)
                player->y = (player->y / 8 + 1) * 8;
        }
        
        if (col & COLLISION_DOWN) {
            if (player->y % 8 != 0)
                player->y = (player->y / 8) * 8;
        }
        
        if (col & COLLISION_LEFT) {
            if (player->x % 8 != 0)
                player->x = (player->x / 8 + 1) * 8;
        }
        
        if (col & COLLISION_RIGHT) {
            if (player->x % 8 != 0)
                player->x = (player->x / 8) * 8;
        }
    }
    
    { // bat col
        i32 dist_x = bat->x - player->x;
        i32 dist_y = bat->y - player->y;
        
        i32 dir = 0;
        
        if (!bat->grabed) {
            if (dist_y < 16 && dist_y > -16) {
                if (dist_x > 0 && dist_x < 17) {
                    col |= COLLISION_RIGHT;
                    dir = 1;
                } else if (dist_x < 0 && dist_x > -17) {
                    col |= COLLISION_LEFT;
                    dir = 2;
                }
            }
            
            if (dist_x < 16 && dist_x > -16) {
                if (dist_y > 0 && dist_y < 17) {
                    col |= COLLISION_DOWN;
                    dir = 3;
                } else if (dist_y < 0 && dist_y > -17) {
                    col |= COLLISION_UP;
                    dir = 4;
                }
            }
        }
        
        if ( keyDown(KEY_A)) {
            switch (bat->grabed) {
                case 0 : { bat->grabed = dir; } break;
                case 1: {
                    bat->grabed = 0;
                    bat->vx = player->vx + 1;
                    bat->vy = player->vy;
                    
                    player->vx--;
                } break;
                case 2: {
                    bat->grabed = 0;
                    bat->vx = player->vx - 1;
                    bat->vy = player->vy;
                    
                    player->vx++;
                } break;
                case 3: {
                    bat->grabed = 0;
                    bat->vx = player->vx;
                    bat->vy = player->vy + 1;
                    
                    player->vy--;
                } break;
                case 4: {
                    bat->grabed = 0;
                    bat->vx = player->vx;
                    bat->vy = player->vy - 1;
                    
                    player->vy++;
                } break;
            }
            
            bat->vx = CLAMP(bat->vx, -1, 1);
            bat->vy = CLAMP(bat->vy, -1, 1);
            player->vx = CLAMP(player->vx, -1, 1);
            player->vy = CLAMP(player->vy, -1, 1);
        }
    }
    
    b32 c_ld = ((col & COLLISION_RIGHT) && bat->grabed != 1)
        || ((col & COLLISION_LEFT) && bat->grabed != 2);
    
    b32 c_ud = ((col & COLLISION_DOWN) && bat->grabed != 3)
        || ((col & COLLISION_UP) && bat->grabed != 4);
    
    if (c_ud) {
        player->vx = input_x;
        if (!c_ld) player->vy = 0;
    }
    if (c_ld) {
        player->vy = input_y;
        if (!c_ud) player->vx = 0;
    }
    
    if (keyDown(KEY_B)) {
        if (col & COLLISION_UP)    player->vy =  1;
        if (col & COLLISION_DOWN)  player->vy = -1;
        if (col & COLLISION_LEFT)  player->vx =  1;
        if (col & COLLISION_RIGHT) player->vx = -1;
        
        player->x += player->vx;
        player->y += player->vy;
    } else {
        if (player->vy < 0 && !(col & COLLISION_UP)) {
            player->y--;
        } else if (player->vy > 0 && !(col & COLLISION_DOWN)) {
            player->y++;
        }
        
        if (player->vx < 0 && !(col & COLLISION_LEFT)) {
            player->x--;
        } else if (player->vx > 0 && !(col & COLLISION_RIGHT)) {
            player->x++;
        }
    }
    
    if (player->x < 0 || player->x > 256 - 16 ||
        player->y < 0 || player->y > 256 - 16)
    {
        player->x = 8;
        player->y = 104;
        
        view->x = 0;
        view->y = 32;
    }
    
    
    // draw
    i32 sprite_i = cube_spr_i;
    if (col & (COLLISION_UP | COLLISION_DOWN)) {
        if (player->vx > 0) sprite_i = cube_spr_right_i;
        else if (player->vx < 0) sprite_i = cube_spr_left_i;
    } else if (col & (COLLISION_RIGHT | COLLISION_LEFT)) {
        if (player->vy > 0) sprite_i = cube_spr_down_i;
        else if (player->vy < 0) sprite_i = cube_spr_up_i;
    }
    
    if (input_x == 0 && input_y == 0) {
        drawSprite(player->x - view->x, player->y - view->y, sprite_i, 0, 1);
    } else {
        drawSprite(player->x - view->x, player->y - view->y,
                   sprite_i + player->frame * 8, 0, 1);
    }
    
    if (player->anim_timer == 8) {
        player->frame++;
        if (player->frame == 3) player->frame = 0;
        player->anim_timer = 0;
    } else {
        player->anim_timer++;
    }
}

static void
updateBattery(Battery *bat, Player *player, View *view)
{
    u32 col = playerCollisionUp(bat->x, bat->y, 0);
    col |= playerCollisionDown(bat->x, bat->y, 0);
    col |= playerCollisionLeft(bat->x, bat->y, 0);
    col |= playerCollisionRight(bat->x, bat->y, 0);
    
    b32 c_ud = col & (COLLISION_UP | COLLISION_DOWN);
    b32 c_ld = col & (COLLISION_LEFT | COLLISION_RIGHT);
    
    if (c_ud) {
        if (!c_ld) bat->vy = 0;
    }
    
    if (c_ld) {
        if (!c_ud) bat->vx = 0;
    }
    
    if (bat->vy < 0 && !(col & COLLISION_UP)) {
        bat->y--;
    } else if (bat->vy > 0 && !(col & COLLISION_DOWN)) {
        bat->y++;
    }
    
    if (bat->vx < 0 && !(col & COLLISION_LEFT)) {
        bat->x--;
    } else if (bat->vx > 0 && !(col & COLLISION_RIGHT)) {
        bat->x++;
    }
    
    switch (bat->grabed) {
        case 1: {
            bat->x = player->x + 16;
            bat->y = player->y;
        } break;
        
        case 2: {
            bat->x = player->x - 16;
            bat->y = player->y;
        } break;
        
        case 3: {
            bat->x = player->x;
            bat->y = player->y + 16;
        } break;
        
        case 4: {
            bat->x = player->x;
            bat->y = player->y - 16;
        } break;
    }
    
    i32 screen_x = bat->x - view->x;
    i32 screen_y = bat->y - view->y;
    if (screen_x > -17 && screen_x < S_WIDTH + 2 &&
        screen_y > -17 && screen_y < S_HEIGHT + 2)
    {
        drawSprite(screen_x, screen_y, battery_spr_i, 0, 1);
    }
}
