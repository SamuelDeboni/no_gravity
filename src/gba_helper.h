#ifndef GBA_HELPER_H
#define GBA_HELPER_H

#include "types.h"

// === Usefull typedefs ===

// ========================

#define MEM_IO       0x04000000
#define MEM_VRAM     0x06000000
#define MEM_OAM      0x07000000
#define MEM_BG_PALETTE  0x05000000
#define MEM_PALETTE     0x05000200

#define REG_DISPCNT  *((volatile u32 *) (MEM_IO+0x0000))
#define REG_VCOUNT   *(volatile u16 *) 0x04000006
#define REG_KEYINPUT *(volatile u32 *) 0x04000130

#define REG_BG0_CONTROL        *((volatile u16 *) (0x04000008))
#define REG_BG1_CONTROL        *((volatile u16 *) (0x0400000A))
#define REG_BG2_CONTROL        *((volatile u16 *) (0x0400000C))
#define REG_BG3_CONTROL        *((volatile u16 *) (0x0400000E))

#define REG_BG0_SCROLL_H       *((volatile u16*) (0x04000010))
#define REG_BG0_SCROLL_V       *((volatile u16*) (0x04000012))
#define REG_BG1_SCROLL_H       *((volatile u16*) (0x04000014))
#define REG_BG1_SCROLL_V       *((volatile u16*) (0x04000016))
#define REG_BG2_SCROLL_H       *((volatile u16*) (0x04000018))
#define REG_BG2_SCROLL_V       *((volatile u16*) (0x0400001A))
#define REG_BG3_SCROLL_H       *((volatile u16*) (0x0400001C))
#define REG_BG3_SCROLL_V       *((volatile u16*) (0x0400001E))

#define vid_mem        ((u16*) MEM_VRAM)

#define tile_mem       ((Tileset  *) MEM_VRAM)
#define tile8_mem      ((Tileset8 *) MEM_VRAM)
#define palette_mem    ((u16 *) MEM_PALETTE)
#define bg_palette_mem ((u16 *) MEM_BG_PALETTE)
#define oam_mem        ((ObjAttr *) MEM_OAM)
#define screen_block_mem ((ScreenBlock *) MEM_VRAM)

// =======================

// Modes
#define DCNT_MODE0   0x0000
#define DCNT_MODE1   0x0001
#define DCNT_MODE2   0x0002
#define DCNT_MODE3   0x0003
#define DCNT_MODE4   0x0004
#define DCNT_MODE5   0x0005

// Layers
#define DCNT_BG0     0x0100
#define DCNT_BG1     0x0200
#define DCNT_BG2     0x0400
#define DCNT_BG3     0x0800
#define DCNT_OBJ     0x1000
#define DCNT_OBJ_1D  0x0040

// ========================

#define S_WIDTH      240
#define S_HEIGHT     160

#define CLR_BLACK   0x0000
#define CLR_RED     0x001F
#define CLR_LIME    0x03E0
#define CLR_YELLOW  0x03FF
#define CLR_BLUE    0x7C00
#define CLR_MAG     0x7C1F
#define CLR_CYAN    0x7FE0
#define CLR_WHITE   0x7FFF

// ===============================

typedef struct {
    u32 data[8];
} Tile, Tile4;

typedef struct {
    u32 data[16];
} Tile8;

typedef Tile  Tileset[512];
typedef Tile8 Tileset8[256];
typedef u16 ScreenBlock[1024];

typedef struct ObjAttr {
    u16 attr0;
    u16 attr1;
    u16 attr2;
    u16 _fill;
} ALIGN4 ObjAttr;

typedef struct ObjAffine{
    u16 _fill0[3];
    i16 pa;
    
    u16 _fill1[3];
    i16 pb;
    
    u16 _fill2[3];
    i16 pc;
    
    u16 _fill3[3];
    i16 pd;
} ALIGN4 ObjAffine;

// Attr defines
#define ATTR0_Y_MASK     0x00ff
#define ATTR0_Y_SHIFT         0
#define ATTR0_Y(n)      ((n) << ATTR0_Y_SHIFT)

#define ATTR0_CM_MASK     0x2000
#define ATTR0_CM_SHIFT        13
#define ATTR0_CM(n)       ((n) << ATTR0_Y_SHIFT)

#define ATTR1_X_MASK     0x01ff
#define ATTR1_X_SHIFT         0
#define ATTR1_X(n)      ((n) << ATTR0_X_SHIFT)

#define ATTR1_VF_MASK    0x2000
#define ATTR1_VF_SHIFT       13
#define ATTR1_VF(n)      ((n) << ATTR1_VF_SHIFT)

#define ATTR1_HF_MASK    0x1000
#define ATTR1_HF_SHIFT       12
#define ATTR1_HF(n)      ((n) << ATTR1_HF_SHIFT)

#define ATTR1_SZ_MASK    0xc000
#define ATTR1_SZ_SHIFT       14
#define ATTR1_SZ(n)      ((n) << ATTR1_SZ_SHIFT)

#define ATTR2_ID_MASK    0x03ff
#define ATTR2_ID_SHIFT        0
#define ATTR2_ID(n)      ((n) << ATTR2_ID_SHIFT)


#define BG_CONTROL_SBB_MASK  0x1f00
#define BG_CONTROL_SBB_SHIFT      8
#define BG_CONTROL_SBB(n)    ((n) << BG_CONTROL_SBB_SHIFT)

#define BG_CONTROL_SZ_MASK   0xc000
#define BG_CONTROL_SZ_SHIFT      14
#define BG_CONTROL_SZ(n)     ((n) << BG_CONTROL_SZ_SHIFT)

// bit field set routines
#define BF_PREP(x, name)         ( ((x)<<name##_SHIFT)& name##_MASK  )
#define BF_GET(x, name)          ( ((x) & name##_MASK)>> name##_SHIFT )
#define BF_SET(y, x, name)       (y = ((y)&~name##_MASK) | BF_PREP(x,name) )

#define BF_PREP2(x, name)        ( (x) & name##_MASK )
#define BF_GET2(y, name)         ( (y) & name##_MASK )
#define BF_SET2(y, x, name)      (y = ((y)&~name##_MASK) | BF_PREP2(x, name) )

static inline void
m3_plot(i32 x, i32 y, Color clr)
{
    vid_mem[y * S_WIDTH + x] = clr;
}

static inline u16
Attr0_Build(u32 y, u32 shape, u32 bpp, u32 mode, u32 mos, u32 bld, u32 win)
{
    u16 result = (y & 255) | ((mode & 3) << 8)
        | ((bld & 1) << 10) | ((win & 1) << 11)
        | ((mos & 1) << 12) | ((bpp & 8)<<10)
        | ((shape & 3) << 14);
    
    return result;
}

static inline u16
Attr1_Build_R(u32 x, u32 size, u32 hflip, u32 vflip)
{
    u16 result = (x & 511) | ((hflip & 1) << 12)
        | ((vflip & 1) << 13) | ((size & 3) << 14);
    return result;
}

static inline u16
Attr1_Build_A(u32 x, u32 size, u32 aff_id)
{
    u16 result = (x & 511) | ((aff_id & 31) << 9) | ((size & 3) << 14);
    return result;
}

static inline u16
Attr2_Build(u32 id, u32 pbank, u32 prio)
{
    u16 result = (id & 0x3FF) | ((pbank & 15) << 12) | ((prio & 3) << 10);
    return result;
}

static void
oamCopy(ObjAttr *dst, const ObjAttr *src, u32 count)
{
#if 1
    while (count--) {
        *dst++ = *src++;
    }
#else
    u32 *dstw = (u32 *)dst;
    u32 *srcw = (u32 *)src;
    while (count --) {
        *dstw++ = *srcw++;
        *dstw++ = *srcw++;
    }
#endif
}

static void
oamInit(ObjAttr *obj, u32 count)
{
    u32 nn = count;
    ObjAttr *dst = obj;
    
    while (nn--) {
        *dst = (ObjAttr){
            .attr0 = 0x0200,
        };
        dst++;
    }
    
    oamCopy(oam_mem, obj, count);
}

// ===============================

#define MAX(x, max) ((x) > (max) ? (x) : (max))
#define MIN(x, min) ((x) < (min) ? (x) : (min))
#define CLAMP(x, min, max) MAX( MIN((x), (max)), (min) )

#define GET_FLAG(x, f) (((x) & (f)) == (f))

// ===============================

static inline Color
rgb16(u32 red, u32 green, u32 blue)
{
    return ( red | (green << 5) | (blue << 10) );
}

static inline void
waitVDraw()
{
    while(REG_VCOUNT >= 160);
}

static inline void
waitVBlank()
{
    while(REG_VCOUNT < 160);
}

static void
vidVsync()
{
    waitVDraw();
    waitVBlank();
}

static inline void
memCopy32(u32 *dst, const u32 *src, u32 count)
{
    while (count --) {
        *dst++ = *src++;
    }
}

#endif //GBA_HELPER_H