/* date = January 22nd 2021 4:22 pm */

#ifndef FIXED_H
#define FIXED_H

#include "types.h"

typedef i32 fix;

#define FIX_SHIFT 16
#define FIX_SCALE (1 << FIX_SHIFT)

#define FIX_ONE (1 << 16)
#define FIX0_5 (1 << 15)
#define FIX0_25 (1 << 14)
#define FIX0_125 (1 << 13)
#define FIX0_0625 (1 << 12)
#define FIX0_03125 (1 << 11)

internal inline u32
fix_to_i32(fix f)
{
    i32 result = (i32) (f / FIX_SCALE);
    return result;
}

internal inline fix
i32_to_fix(i32 i)
{
    fix result = (fix) (i << FIX_SHIFT);
    return result;
}

internal fix
fix_mul(fix a, fix b)
{
    fix result = (a * b) >> FIX_SHIFT;
    return result;
}

internal fix
fix_div(fix a, fix b)
{
    fix result = (a * FIX_SCALE) / b;
    return result;
}

internal inline fix
fix_sign(fix f)
{
    return i32_to_fix((f > 0) - (f < 0));
}

internal inline fix
fix_abs(fix f) {
    return fix_sign(f) > 0 ? f : -f;
}

#undef FIX_SHIFT
#undef FIX_SCALE

#endif //FIXED_H
