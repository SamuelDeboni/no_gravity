#ifndef SOUND_H
#define SOUND_H

#define NO_NOTE 0xff
#define PAUSE   0xfe
#define C       0x00
#define Cs      0x01
#define D       0x02
#define Ds      0x03
#define E       0x04
#define F       0x05
#define Fs      0x06
#define G       0x07
#define Gs      0x08
#define A       0x09
#define As      0x0a
#define B       0x0b

#define OCT_0 0x00
#define OCT_1 0x10
#define OCT_2 0x20

#include "registers.h"
#include "mem_def.h"

const u32 __snd_rates[12]=
{
    8013, 7566, 7144, 6742, // C , C#, D , D#
    6362, 6005, 5666, 5346, // E , F , F#, G
    5048, 4766, 4499, 4246  // G#, A , A#, B
};

#define SND_RATE(note, oct) ( 2048-(__snd_rates[note]>>(4+(oct))) )
#define SND_INIT_DELAY 60

//GLOBAL STATE OF SOUND
u8 _sound_init_delay_counter;

u8 *_actual_sound_eff;
u16 _actual_sound_eff_tick;
u16 _actual_sound_eff_tick_qnt;

u16 _sound_index_ch1;
u16 _sound_tick_number_ch1;

u16 _sound_index_ch2;
u16 _sound_tick_number_ch2;

u16 _sound_index_ch4;
u16 _sound_tick_number_ch4;

u8 _pause_status;

#define PAUSE_CH_1 0b00000001
#define PAUSE_CH_2 0b00000010

#include "sound/musics.h"
#include "sound/sfx.h"

static inline void
initSound()
{
    // Cleaning state
    _sound_init_delay_counter = 0;

    _actual_sound_eff = 0;
    _actual_sound_eff_tick = 0;
    _actual_sound_eff_tick_qnt = 0;

    _sound_index_ch1 = 0;
    _sound_tick_number_ch1 = 0;
    _sound_index_ch2 = 0;
    _sound_tick_number_ch2 = 0;
    _sound_index_ch4 = 0;
    _sound_tick_number_ch4 = 0;
   
    _pause_status = 0;

    // turn sound on
    REG_SNDSTAT= SSTAT_ENABLE;
    
    // snd1 on left/right ; both full volume
    REG_SNDDMGCNT = SDMG_BUILD_LR(SDMG_SQR1 | SDMG_SQR2 | SDMG_WAVE | SDMG_NOISE, 7);
    
    // DMG ratio to 100%
    REG_SNDDSCNT= SDS_DMG100;
    
    // CHANNEL 1
    
    // no sweep
    REG_SND1SWEEP= SSW_OFF;
    
    // envelope: vol=12, decay, max step time (7) ; 50% duty
    REG_SND1CNT = SSQR_ENV_BUILD(12, 0, 1) | SSQR_DUTY1_2;
    REG_SND1FREQ = 0;
    
    // CHANNEL 2
    
    // envelope
    REG_SND2CNT = SSQR_ENV_BUILD(12, 0, 4) | SSQR_DUTY3_4;
    REG_SND2FREQ = 0;

    // CHANNEL 4
    
    // envelope
    REG_SND4CNT = SSQR_ENV_BUILD(12, 0, 1) | SSQR_DUTY1_2;
    REG_SND4FREQ = 0;
}

// Play a note and show which one was played
static inline void
notePlayCh2(int note, int octave) {
    // Play the actual note
    REG_SND2FREQ = SFREQ_RESET | SND_RATE(note, octave);
}

static inline void
playNotesCh1() {
    
    _sound_tick_number_ch1++;
    
    if (_sound_tick_number_ch1 >=
        notes_ch1[_sound_index_ch1][1]) {
        
        _sound_tick_number_ch1 = 0;
        _sound_index_ch1++;
        
        if (_sound_index_ch1 >= notes_ch1_qnt) {
            _sound_index_ch1 = 0;
        }
    }
    
    // Play the actual note.
    if (_actual_sound_eff == 0) {
        if (notes_ch1[_sound_index_ch1][0] != NO_NOTE) {
            
           REG_SND1CNT = 
                SSQR_ENV_BUILD(12,
                               0,
                               notes_ch1[_sound_index_ch1][2]) | SSQR_DUTY1_2;
            
            REG_SND1FREQ = SFREQ_RESET |
                SND_RATE(notes_ch1[_sound_index_ch1][0]&15,
                         notes_ch1[_sound_index_ch1][0]>>4);
        }
    } else {
        if (_actual_sound_eff[_actual_sound_eff_tick] != NO_NOTE) {
            REG_SND1FREQ = SFREQ_RESET |
                    SND_RATE(_actual_sound_eff[_actual_sound_eff_tick]&15,
                             _actual_sound_eff[_actual_sound_eff_tick]>>4);
            }

        _actual_sound_eff_tick++;
        if (_actual_sound_eff_tick >= _actual_sound_eff_tick_qnt) {
            _actual_sound_eff = 0;
            _actual_sound_eff_tick = 0;
        }
    }
}

static inline void
playNotesCh2() {
    
    _sound_tick_number_ch2++;
    
    if (_sound_tick_number_ch2 >=
        notes_ch2[_sound_index_ch2][1]) {
        
        _sound_tick_number_ch2 = 0;
        _sound_index_ch2++;
        
        if (_sound_index_ch2 >= notes_ch2_qnt) {
            _sound_index_ch2 = 0;
        }
    }
    
    // Play the actual note.
    if (notes_ch2[_sound_index_ch2][0] != NO_NOTE) {
        REG_SND2FREQ = SFREQ_RESET |
            SND_RATE(notes_ch2[_sound_index_ch2][0]&15,
                     notes_ch2[_sound_index_ch2][0]>>4);
    }
}

static inline void
playNotesCh4() {
    
    _sound_tick_number_ch4++;
    
    if (_sound_tick_number_ch4 >=
        notes_ch4[_sound_index_ch4][1]) {
        
        _sound_tick_number_ch4 = 0;
        _sound_index_ch4++;
        
        if (_sound_index_ch4 >= notes_ch4_qnt) {
            _sound_index_ch4 = 0;
        }
    }
    
    // Play the actual note.
    if (notes_ch4[_sound_index_ch4][0] != NO_NOTE) {
        REG_SND4FREQ = SFREQ_RESET |
            SND_RATE(notes_ch4[_sound_index_ch4][0]&15,
                     notes_ch4[_sound_index_ch4][0]>>4);
    }
}

#endif