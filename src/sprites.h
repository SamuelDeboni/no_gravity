/* date = January 16th 2021 3:01 pm */

#ifndef SPRITES_H
#define SPRITES_H

#include "types.h"

#include "sprites/MaintenanceCube.h"

#include "sprites/ShipTS.h"
#include "sprites/EmergencyBattery.h"

#include "sprites/Screen_1.h"
#include "sprites/Screen_2.h"
#include "sprites/Level_test_0.h"
#include "sprites/Level_III.h"
#include "sprites/Level_IV.h"

#include "sprites/palette.h"

#endif
