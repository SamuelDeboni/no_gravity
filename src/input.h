/* date = January 14th 2021 1:37 pm */

#ifndef INPUT_H
#define INPUT_H

#include "gba_helper.h"

#define KEY_A        0x0001
#define KEY_B        0x0002
#define KEY_SELECT   0x0004
#define KEY_START    0x0008
#define KEY_RIGHT    0x0010
#define KEY_LEFT     0x0020
#define KEY_UP       0x0040
#define KEY_DOWN     0x0080
#define KEY_R        0x0100
#define KEY_L        0x0200

#define KEY_MASK     0x03FF

u16 key_current = 0;
u16 key_previus = 0;

static inline void
keyPoll() {
    key_previus = key_current;
    key_current = (~REG_KEYINPUT) & KEY_MASK;
}

static inline u32
keyPressed(u32 key) {
    return (key_current & key) == key;
}

static inline u32
keyUp(u32 key) {
    return ((key_previus & key) && (~key_current & key));
}

static inline u32
keyDown(u32 key) {
    return ((~key_previus & key) && (key_current & key));
}

static inline i32
bitTribool(u32 x, u32 plus, u32 minus) {
    return (i32)((x & plus) == plus) - (i32)((x & minus) == minus);
}

#define AXIS_HORIZONTAL 0
#define AXIS_VERICAL 1

static i32
keyAxis(u32 axis) {
    switch (axis) {
        case AXIS_HORIZONTAL: {
            return (i32)keyPressed(KEY_RIGHT) - (i32)keyPressed(KEY_LEFT);
        } break;
        
        case AXIS_VERICAL: {
            return (i32)keyPressed(KEY_DOWN) - (i32)keyPressed(KEY_UP);
        } break;
    }
    
    return 0;
}

#endif //INPUT_H
